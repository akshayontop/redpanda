import xmlrpclib
import datetime


class Odoo():
    def __init__(self):
        self.url = "http://localhost:8069"
        self.db = "koko1"
        self.username = "a.bhadange@kokonetworks.com"
        self.password = "password"
        self.common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(self.url))
        self.object = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(self.url))

    def authenticationOdoo(self):
        self.common = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(self.url))
        self.models = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(self.url))
        self.uid = self.common.authenticate(self.db, self.username, self.password, {})

    def contactAdd(self, data):
        contact_id = self.models.execute_kw(self.db, self.uid, self.password, 'res.partner', 'create', data)
        return contact_id

    def contactCheck(self, name):
        name_filter = [[("name", '=', name)]]
        contact_id = self.models.execute_kw(self.db, self.uid, self.password, 'res.partner', 'search', name_filter)
        return contact_id

    def getAllFields(self):
        data = self.models.execute_kw(self.db, self.uid, self.password, 'res.partner', 'fields_get', [],
                                      {'attributes': ['string', 'help', 'type']})
        return data

    # def deleteContact(self, data):
    #     id = self.models.execute_kw(self.db, self.uid, self.password, 'res.partner', 'create',
    #         [{
    #             "name": "ABC Company",
    #             "street": "Viman Nagar",
    #             "phone": "0217-2724847",
    #             "mobile": "8855224466",
    #             "website": "theredpandas.com"
    #
    #         }])


def main():
    o1 = Odoo()
    o1.authenticationOdoo()
    print o1.uid
    data = [{"name": "RED PANDA",
             "street": "Kalyani Nagar",
             "phone": "0217-2724847",
             "mobile": "8855224466",
             "website": "theredpandas.com"
             }]
    # o1.contactAdd(data)

    res = o1.contactCheck("RED PANDA")
    print res

    fields = o1.getAllFields()
    # print fields

    o1.deleteContact(data)

    print "Done !"


if __name__ == '__main__':
    main()
